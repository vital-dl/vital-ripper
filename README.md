# vitalsource ripper

in order to make full use of this, you will have to do some sniffing in network tools.

for the technically inclined - populate config.js with the values from -
`https://${baseURL}/books/${BOOKID}/pages/${startPage}/content?brand=vitalsource&create=true`

find a request that starts with `content?brand=...&create=true`
`cookie` will start with and include `_jigsaw_session=`
`bookID` from `/reader/books/BOOKID/pageid/0`
`startPage` is not zero-indexed value after pageid, grab it from the request, it follows `/BOOKID/pages/${pageNumber}`
navigate to the end of the book and also grab the last page number as `endPage`
`encKey` is the private key used to decrypt the file, if you use ctrl+f in network and search for `encryptionKeyBuffer` you'll find the key there