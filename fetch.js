const fs = require('fs');
const path = require('path');
const axios = require("axios")
const htmlparser2 = require("htmlparser2");
const domutils = require("domutils")
const { decrypt } = require("./decryptImg.js")
const { baseURL, cookie, bookID, startPage, endPage } = require("./config.js.example")

const client = axios.create({
  method: "GET",
  headers: {
    'Accept': 'text/html, */*; q=0.01 gzip, deflate, br en-US,en;q=0.5',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'Accept-Language': 'en-US',
    'Origin': baseURL,
  }
})
function getEncrypted(cookie, bookID, pageNo) {
  const url = baseURL + `/books/${bookID}/pages/${pageNo}/content?brand=vitalsource&create=true`
  return client({
    url,
    headers: {
      'Cookie': cookie,
      'Referer': url
    }
  }).then(res => {
    const dom = htmlparser2.parseDocument(res.data);
    const filter = (e) => e.attribs.id == "page-content"
    const result = domutils.findOne(filter, dom.children, true)
    const encoded = result.children[0].data.trim()
    return encoded
  }).catch(err => {
    console.log(err)
  })
}

const downloadFile = async (cookie, fileUrl, fileName) => {
  const localFilePath = path.resolve(__dirname, "img", fileName);
  client({
    url: baseURL + fileUrl,
    responseType: 'stream',
    headers: {
      'Cookie': cookie,
    }
  }).then(res =>
    res.data.pipe(fs.createWriteStream(localFilePath))
    .on('finish', () => console.log(`downloaded ${fileName}`))
  )
};


const sleep = async (ms) => new Promise(resolve => setTimeout(resolve, ms))
const randSleep = () => Math.ceil(Math.random() * (10 - 2) + 2) * 1000
const log = (msg) => { console.log(msg); return msg }

async function getBook(cookie, bookID, startPage, endPage) {
  for (let i = startPage; i <= endPage; i++) {
    await getEncrypted(cookie, bookID, i)
      .then(enc => decrypt(enc, i))
      //.then(log)
      .then(res => downloadFile(cookie, `${res.url}2000`, `${res.pageNo}.jpg`))
    await sleep(randSleep())
  }
}

getBook(cookie, bookID, startPage, endPage)