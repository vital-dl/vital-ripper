const { Crypto } = require("@peculiar/webcrypto");
const window = { crypto: new Crypto() }

const { encKey } = require("./config.js.example")

const importSecretKey = async (keyBuffer) => window.crypto.subtle.importKey("raw", keyBuffer, "AES-GCM", true, ["encrypt", "decrypt"]);

const btoab = (b64Text) => Uint8Array.from(atob(b64Text), c => c.charCodeAt(0)).buffer

const decryptMessage = async (privkey, ciphertext, iv) =>
  window.crypto.subtle.decrypt({
    iv,
    name: "AES-GCM",
    tagLength: 128
  }, privkey, ciphertext);

var decryptWithKey = async function (pagecontent) {
  const key = await importSecretKey(encKey);
  const [ivString, b64CipherText] = pagecontent.split(":");
  const iv = new Uint8Array(ivString.split(","));
  const ciphertext = btoab(b64CipherText);
  const decryptAwait = await decryptMessage(key, ciphertext, iv);
  return decryptAwait
};

const decrypt = async (pageContent, pageNo) => 
  decryptWithKey(pageContent)
    .then(result => {
      const b64 = new TextDecoder("utf-16").decode(result)
      const string = atob(b64);
      const imgSrc = new RegExp(/<img id="pbk-page" src="(\/books\/[\d\w]+\/images\/[a-f0-9]+\/encrypted\/)/,"")
      const end = string.match(imgSrc)[1]
      return { url: end, pageNo }
    })

module.exports = {
  decrypt
}