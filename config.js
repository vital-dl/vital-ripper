const encKey = new Uint8Array([]) // fill this out yourself
const cookie = "" // _jigsaw_session cookie
const bookID = "" // /reader/books/${BOOKID}/pageid/
const startPage = 1000 // rarely starts at 0 or 1, varies by book, if first page is inner cover, decrease by 1 to get image cover
const endPage = 15000 
const baseURL = "https://example.com"